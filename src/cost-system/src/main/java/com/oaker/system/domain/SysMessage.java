package com.oaker.system.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.oaker.common.annotation.Excel;
import com.oaker.common.core.domain.BaseEntity;

/**
 * 消息对象 sys_message
 * 
 * @author ruoyi
 * @date 2023-11-23
 */

@Data
public class SysMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 消息id */
    private Integer messageId;

    /** 消息标题 */
    @Excel(name = "消息标题")
    private String title;

    /** 消息来源业务id */
    @Excel(name = "消息来源业务id")
    private long sourceId;

    /** 信息内容 */
    @Excel(name = "信息内容")
    private String messageContent;

    /** 信息状态（0未读 1已读） */
    @Excel(name = "信息状态" )
    private Integer status;

    /** 接收用户id */
    @Excel(name = "接收用户id")
    private long receiveUserid;


    @Override
    public String toString() {
        return "SysMessage{" +
                "messageId=" + messageId +
                ", title='" + title + '\'' +
                ", sourceId='" + sourceId + '\'' +
                ", messageContent='" + messageContent + '\'' +
                ", status='" + status + '\'' +
                ", receiveUserid=" + receiveUserid +
                '}';
    }
}
